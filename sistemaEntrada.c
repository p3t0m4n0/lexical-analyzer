/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   sistemaEntrada.h
 * Author: roberto
 *
 * Created on 23 de octubre de 2018, 12:27
 */

#include <stdio.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>
#include "sistemaEntrada.h"

#define tamanhoBufer 129

FILE* file;
char* buffer1;
char* buffer2;
int flag = 0, lastBuffer;
char *delantero = 0, *inicio = 0;

void cargarBufer() {
    int count;
    if (lastBuffer == 2) {
        count = fread(buffer1, sizeof (char), tamanhoBufer - 1, file);
        if (count < tamanhoBufer - 1) {
            *(buffer1 + count) = EOF;
        }
        delantero = buffer1;
        lastBuffer = 1;
    } else if (lastBuffer == 1) {
        count = fread(buffer2, sizeof (char), tamanhoBufer - 1, file);
        if (count < tamanhoBufer - 1) {
            buffer2[count] = EOF;
        }
        delantero = buffer2;
        lastBuffer = 2;
    }
}

void iniciar() {
    lastBuffer = 2;
    buffer1 = (char*) malloc(tamanhoBufer * sizeof (char));
    buffer2 = (char*) malloc(tamanhoBufer * sizeof (char));
    inicio = buffer1;
    *(buffer1 + tamanhoBufer - 1) = EOF;
    *(buffer2 + tamanhoBufer - 1) = EOF;
    cargarBufer();
}

int bufer_1() {
    int buffer;
    if (inicio >= buffer1 && inicio <= (buffer1 + tamanhoBufer - 1)) {
        buffer = 1;
    } else if (inicio >= buffer2 && inicio <= (buffer2 + tamanhoBufer - 1))
        buffer = 2;
    return buffer;
}

int bufer_2() {
    int buffer;
    if (delantero >= buffer1 && delantero <= (buffer1 + tamanhoBufer - 1)) {
        buffer = 1;
    } else if (delantero >= buffer2
            && delantero <= (buffer2 + tamanhoBufer - 1)) {
        buffer = 2;
    }
    return buffer;
}

int bufer() {
    int ret;
    if (bufer_1() == bufer_2()) {
        ret = 1;
    } else {
        ret = 0;
    }
    return ret;
}

char siguienteCaracter() {
    char ret;
    if (lastBuffer == 2) {
        if (*delantero == EOF) {
            if (delantero == (buffer2 + tamanhoBufer - 1)) {
                if (bufer()) {
                    if (bufer_2() == 2) {
                        cargarBufer();
                    } else {
                        delantero = buffer2;
                    }
                }
            } else {
                ret = EOF;
            }
        }
    } else if (lastBuffer == 1) {
        if (*delantero == EOF) {
            if (delantero == (buffer1 + tamanhoBufer - 1)) {
                if (bufer()) {
                    if (bufer_2() == 1) {
                        cargarBufer();
                    } else {
                        delantero = buffer1;
                    }
                }
            } else {
                ret = EOF;
            }
        }
    }
    ret = *delantero;
    if (flag == 1) {
        flag = 0;
        inicio = delantero;
    }
    delantero = delantero + 1;

    return ret;
}

void retroceder() {
    delantero = inicio;
}

void volverAtras() {
    if (delantero != buffer1 && delantero != buffer2) {
        delantero = delantero - 1;
    } else if (lastBuffer == 1) {
        delantero = buffer2 + tamanhoBufer - 1;
    } else if (lastBuffer == 2) {
        delantero = buffer1 + tamanhoBufer - 1;
    }
    flag = 1;
}

FILE* abrirArchivo(char* ruta) {

    file = fopen(ruta, "rt");
    return file;
}

char* enviarComponente() {
    char *ret, *tmp;
    int i;
    tmp = inicio;
    if (bufer()) {
        ret = (char*) malloc(sizeof (delantero - inicio + 1) * sizeof (char));
        for (i = 0; tmp < delantero; i++) {
            ret[i] = *tmp;
            tmp += 1;
        }
        ret[i] = '\0';
    } else {
        ret = (char*) malloc((tamanhoBufer - 1) * sizeof (char));
        for (i = 0; tmp != (delantero); i++) {
            if (*tmp != EOF) {
                ret[i] = *tmp;
                tmp += 1;
            } else if (bufer_1() == 1) {
                tmp = buffer2;
                i-=1;
            } else {
                tmp = buffer1;
                i-=1;
            }

        }
        ret[i + 1] = '\0';
    }
    flag = 1;
    return ret;
}

int cerrarFichero() {
    fclose(file);
    return 0;
}

