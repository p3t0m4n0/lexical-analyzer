/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   sistemaEntrada.h
 * Author: roberto
 *
 * Created on 23 de octubre de 2018, 12:27
 */

#include <stdio.h>
#include <stdlib.h>
#include "sistemaEntrada.h"
#include "tablaSimbolos.h"
#include "definiciones.h"
#include "lexico.h"

/*
 * 
 */



int main(int argc, char** argv) {
    
    FILE *file = abrirArchivo("wilcoxon.py");
    iniciar();
    crearTabla(32);
    pedirComponente();
    imprimir();
    cerrarFichero();
    
    return (EXIT_SUCCESS);
}
