/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   definiciones.h
 * Author: roberto
 *
 * Created on 24 de octubre de 2018, 11:18
 */

#define IMPORT 300
#define AS 301
#define DEF 302
#define FOR 303
#define IN 304
#define IF 305
#define ELIF 306
#define ELSE 307
#define NOT 308
#define PRINT 309
#define RETURN 310
#define IDENTIFIER 350
#define MULTILINE_STRING 351
#define INTEGER 352
#define FLOAT 353
#define STRING 356
#define INDENTATION 357
#define POWER 358
#define PLUSPLUS 959
#define MORE_EQUALS 360
#define EQUALS 361
#define MINUSMINUS 362
#define MINUS_EQUALS 363

char* enteroACadena (int id);

