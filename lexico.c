/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   sistemaEntrada.h
 * Author: roberto
 *
 * Created on 23 de octubre de 2018, 12:27
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "sistemaEntrada.h"
#include "lexico.h"
#include "definiciones.h"
#include "tablaSimbolos.h"
#include "tablaHash.h"


char sig = '0';
char anterior;
componenteLexico sigComp;
int numChar;
int linea = 1;

void siguiente() {
    anterior = sig;
    sig = siguienteCaracter();
    numChar++;
}

void pedirComponente() {
    componenteLexico comp;
    int i = 0;
    do {
        comp = siguienteComponente();
        i++;
        fflush(stdout);
    } while (*(comp.lexema) != EOF);
}

void comentarios() {

    sig = siguienteCaracter();
    while (sig != '\n') {
        sig = siguienteCaracter();
    }
    linea++;
    volverAtras();
    numChar = 0;
}

void espacios() {
    sig = siguienteCaracter();
    while (sig == ' ') {
        sig = siguienteCaracter();
    }
    volverAtras();
    numChar = 0;
}

int numeroLinea() {
    return linea;
}

void crearComponente(int tipo) {
    sigComp.id = tipo;
    sigComp.lexema = (char*) malloc((numChar + 1) * sizeof (char));
    strcpy(sigComp.lexema, enviarComponente());
}

void cadena() {
    char primero;
    siguiente();
    primero = sig;
    if (primero == '"' || primero == '\'') {
        do {
            siguiente();
        } while (sig != primero && (anterior != '\'' || anterior != '\\'));
        crearComponente(STRING);
    }
}

void cadena2() {
    int number = 0;
    siguiente();
    if (sig == '\"') {
        siguiente();
        if (sig == '\"') {
            while (number < 3) {
                siguiente();
                if (sig == '\"') {
                    if (anterior == sig) {
                        number += 1;
                    } else {
                        number = 1;
                    }
                }
            }
            if (number == 3) {
                crearComponente(MULTILINE_STRING);
            }
        } else {
            retroceder();
            numChar = 0;
            cadena();
        }
    } else {
        retroceder();
        numChar = 0;
        cadena();
    }
}

void alfanumerico() {
    do {
        siguiente();
    } while (isalpha(sig) || isdigit(sig) || sig == '_');
    volverAtras();
    sigComp.lexema = (char*) malloc((numChar) * sizeof (char));
    strcpy(sigComp.lexema, enviarComponente());

    if (!buscarEnTabla(sigComp.lexema)) {
        insertarEnTabla(sigComp.lexema);
    }

}

void exponentes() {
    siguiente();
    if (sig == '-' || isdigit(sig)) {
        do {
            siguiente();
        } while (isdigit(sig));
    }
    volverAtras();
    crearComponente(FLOAT);
}

void decimales() {
    int tmp = numChar;
    do {
        siguiente();
    } while (isdigit(sig));
    if (numChar > tmp + 1) {
        if (sig == 'e') {
            exponentes();
        } else {
            volverAtras();
            numChar-=1;
            crearComponente(FLOAT);
        }
    } else {
        volverAtras();
        numChar-=1;
        crearComponente(FLOAT);
    }

}

void enteros() {
    do {
        siguiente();
    } while (isdigit(sig));
    if (sig == '.') {
        decimales();
    } else if (sig == 'e') {
        exponentes();
    } else {
        volverAtras();
        crearComponente(INTEGER);
    }
}

int letrasHexa() {
    if (sig == 'a' || sig == 'b' || sig == 'c' || sig == 'd' || sig == 'e' || sig == 'f') {
        return 1;
    } else {
        return 0;
    }
    return 0;
}

void hexadecimal() {
    siguiente();

    if (sig == 'x') {
        do {
            siguiente();
        } while (isdigit(sig) || letrasHexa());
        volverAtras();
        if (strcmp(enviarComponente(), "0x") == 0) {
            printf("error");
        } else {
            crearComponente(INTEGER);
        }
    } else if (isdigit(sig)) {
        enteros();
    } else if (sig == '.') {
        decimales();
    } else if (sig == 'e') {
        exponentes();
    } else {
        volverAtras();
        crearComponente(INTEGER);
    }
}

void numeros() {
    if (sig == '0') {
        hexadecimal();
    } else if (sig == '.') {
        decimales();
    } else {
        enteros();
    }
}

void simbolos() {
    sigComp.lexema = (char*) malloc((numChar + 1) * sizeof (char));
    strcpy(sigComp.lexema, enviarComponente());
    sigComp.id = sigComp.lexema[0];
}

componenteLexico siguienteComponente() {
    int control = 0;
    while (control == 0) {
        numChar = 0;
        siguiente();
        if (sig == '#') {
            comentarios();
        } else if (sig == EOF) {
            simbolos();
            return sigComp;
        } else if (sig == '\"') {
            cadena2();
            return sigComp;
        } else if (sig == '\'') {
            volverAtras();
            cadena();
            return sigComp;
        } else if (sig == '*') {
            siguiente();
            if (sig == '*') {
                crearComponente(POWER);
                return sigComp;
            } else {
                volverAtras();
                numChar-=1;
                simbolos();
                return sigComp;
            }
        } else if (sig == '=') {
            siguiente();
            if (sig == '=') {
                crearComponente(EQUALS);
                return sigComp;
            } else {
                volverAtras();
                numChar-=1;
                simbolos();
                return sigComp;
            }
        } else if (sig == '+') {
            siguiente();
            if (sig == '+') {
                crearComponente(PLUSPLUS);
                return sigComp;
            } else if (sig == '=') {
                crearComponente(MORE_EQUALS);
                return sigComp;
            } else {
                volverAtras();
                numChar-=1;
                simbolos();
                return sigComp;
            }
        } else if (sig == '-') {
            siguiente();
            if (sig == '-') {
                crearComponente(MINUSMINUS);
                return sigComp;
            } else if (sig == '=') {
                crearComponente(MINUS_EQUALS);
                return sigComp;
            } else {
                volverAtras();
                numChar-=1;
                simbolos();
                return sigComp;
            }
        } else if (sig == '\n') {
            linea++;
            simbolos();
            return sigComp;
        } else if (isdigit(sig)) {
            numeros();
            return sigComp;
        } else if (isalpha(sig) || sig == '_') {
            alfanumerico();
            return sigComp;
        } else if (ispunct(sig)) {
            if (sig == '.') {
                siguiente();
                if (isdigit(sig)) {
                    retroceder();
                    numChar = 0;
                    siguiente();
                    numeros();
                    return sigComp;
                } else {
                    volverAtras();
                    numChar-=1;
                }
            }
            simbolos();
            return sigComp;
        } else if (sig == ' ') {
            if (anterior == '\n') {
                do {
                    siguiente();
                } while (sig == ' ');
                volverAtras();
                numChar-=1;
                crearComponente(INDENTATION);
                return sigComp;
            } else {
                espacios();
            }
        }
    }

    return sigComp;
}

